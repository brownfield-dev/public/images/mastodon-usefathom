# Mike's BFD mastodon fork

This repository is built to support running [https://bfd.so](https://bfd.so) by adding 2 types of telemetry.

1. ~~Open Telemtry for observability using [Honeycomb.io](https://honeycomb.io)~~
1. [Fathom Analytics](https://usefathom.com) for general analytics (GDPR compliant)

Also increases the max chars to 5,000.

## Pipeline jobs

The build job builds the container and tags it as both the sha and the branch name.
This allows future jobs to retag the specific image but also allows deploying a specific branch. With a `imagePullPolicy: Always` it can redeploy without changing the image tag each time. Just kill the pod and it'll get the newest.

## Open Telemetry

OpenTelemetry is now part of the main codebase so this hack is no longer needed.

## Fathom 

You can see in the `patch-usefathom.sh` file there are 2 activites.

1. Add the script tag to the header
1. Update the CSP to allow the usefathom script to load
