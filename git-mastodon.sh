#!/bin/bash
cd workdir
git config --global user.email "ci@bfd.so"
git config --global user.name "robot"
git clone https://github.com/mastodon/mastodon.git
cd mastodon
git checkout v4.2.12
git am /workdir/0003-the-other-bfd-hacks.patch
