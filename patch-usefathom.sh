#!/bin/bash
SCRIPT_TAG='%script{:src => "https://cdn.usefathom.com/script.js", "data-site" => "ETMIOBFM", :type => "text/javascript", "data-spa" => "auto", "defer" => "defer" }'
ESCAPED_SCRIPT_TAG=$(printf '%s\n' "$SCRIPT_TAG" | sed -e 's/[]\/$*.^[]/\\&/g');
sed -ie "/header_tags/a \ \ \ \ $ESCAPED_SCRIPT_TAG" app/views/layouts/application.html.haml

CSP_ADDITION=https://cdn.usefathom.com
ESCAPED_CSP=$(printf '%s\n' "$CSP_ADDITION" | sed -e 's/[]\/$*.^[]/\\&/g');
sed -i "s/p.script_src  :self, assets_host/p.script_src  :self, assets_host, \"$ESCAPED_CSP\"/g" config/initializers/content_security_policy.rb

ESCAPED_S3=https:\/\/files\.bfd\.so
sed -i "s/connect_src :self, :data, :blob, assets_host/connect_src :self, :data, :blob, assets_host, \"$ESCAPED_S3\"/g" config/initializers/content_security_policy.rb 
